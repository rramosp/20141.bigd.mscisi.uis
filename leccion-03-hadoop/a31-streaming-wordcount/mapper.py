#!/opt/anaconda/bin/python

import sys, json

for line in sys.stdin:
    record = json.loads(line)
    key = record[0]
    value = record[1]
    words = value.split()
    for w in words:
      if len(w)!=0:
          print "{0}\t{1}".format(w, 1)


