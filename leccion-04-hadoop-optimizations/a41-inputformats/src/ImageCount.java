package uis.bigdata;

import java.io.IOException;
import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.io.BytesWritable;
import org.apache.hadoop.mapreduce.Job;
import org.apache.hadoop.mapreduce.Mapper;
import org.apache.hadoop.mapreduce.Reducer;
import org.apache.hadoop.mapreduce.lib.output.FileOutputFormat;
import org.apache.hadoop.util.GenericOptionsParser;
import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;
import java.io.InputStream;
import java.io.ByteArrayInputStream;

public class ImageCount
{
    public static class SimpleMapper extends Mapper<Text, BytesWritable, Text, Text>
    {
        public void map(Text key, BytesWritable value, Context context) throws IOException, InterruptedException
        {
                 // to codigo aqui:
                 //   recupera la imagen desde el parametro "value"
                 //   emite clave,valor = (nombre de fichero, cadena con tamaño en bytes de la image , alto y ancho de la image
                 //   sugerencia ... usa la libreria ImageIO
        }
    }
    
    public static class SimpleReducer
    extends Reducer<Text,BytesWritable,Text,Text>
    {
        public void reduce(Text key, Iterable<Text> values, Context context) throws IOException, InterruptedException
        {
            context.write(key, values.iterator().next());
        }
    }
    public static void main(String[] args) throws Exception
    {
        Configuration conf = new Configuration();
        Job job = new Job(conf, "imagecount");
        job.setJarByClass(ImageCount.class);
        job.setMapperClass(SimpleMapper.class);
        job.setReducerClass(SimpleReducer.class);
        job.setOutputKeyClass(Text.class);
        job.setOutputValueClass(Text.class);
        job.setInputFormatClass(WholeFileInputFormat.class);
        WholeFileInputFormat.addInputPath(job, new Path(args[0]));
        FileOutputFormat.setOutputPath(job, new Path(args[1]));
        System.exit(job.waitForCompletion(true) ? 0 : 1);
    }
}
