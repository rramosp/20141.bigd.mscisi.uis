package uis.bigdata;

import org.apache.hadoop.mapreduce.RecordReader;
import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.io.NullWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.io.BytesWritable;
import org.apache.hadoop.mapreduce.lib.input.FileSplit;
import org.apache.hadoop.io.IOUtils;
import org.apache.hadoop.mapreduce.TaskAttemptContext;
import org.apache.hadoop.fs.*;
import org.apache.hadoop.mapreduce.InputSplit;
import java.io.*;

class WholeFileRecordReader extends RecordReader<Text, BytesWritable> {
	private FileSplit fileSplit; 
	private Configuration conf; 
	private boolean processed = false;
	
	private Text currentKey = null;
	private BytesWritable currentValue = null;

	public void initialize(InputSplit split,TaskAttemptContext taskAttempt) throws IOException {
		this.fileSplit = (FileSplit)split;
		this.conf = taskAttempt.getConfiguration(); 
	}

	public WholeFileRecordReader(FileSplit fileSplit, TaskAttemptContext taskAttempt) throws IOException {
		this.fileSplit = fileSplit;
		this.conf = taskAttempt.getConfiguration(); 
	}

    @Override
    public Text getCurrentKey() {
		return this.currentKey;
	}
	
	@Override
	public BytesWritable getCurrentValue() {
		return this.currentValue;
	}

	@Override
	public float getProgress() throws IOException {
		return processed ? 1.0f : 0.0f; 
	}

	@Override
	public boolean nextKeyValue() throws IOException {
		if (!processed) {
			byte[] contents = new byte[(int) fileSplit.getLength()]; 
			Path file = fileSplit.getPath();
			FileSystem fs = file.getFileSystem(conf); 
			FSDataInputStream in = null;
			try {
				in = fs.open(file);
				IOUtils.readFully(in, contents, 0, contents.length); 
				currentKey = new Text(file.getName());
				currentValue = new BytesWritable();
				currentValue.set(contents, 0, contents.length);
			} finally {
				IOUtils.closeStream(in); }
				processed = true;
				return true; 
			}
		return false; 
	}
	
	@Override
	public void close() throws IOException {
		// do nothing 
	}
}
