import numpy as np
import time
from nearpy import Engine
from nearpy.hashes import RandomBinaryProjections
from nearpy.hashes import RandomDiscretizedProjections
from nearpy.filters import NearestFilter, UniqueFilter

def create_random_data(number_of_points=5000, number_of_dimensions=100):
    return np.random.randn(number_of_points, number_of_dimensions)

def insert_data(data, engine):
    for index in range(data.shape[0]):
        engine.store_vector(data[index], 'data_%d' % index)

print "-----"
print "----- LSH EXAMPLE CODE ------"

# we need to find 10 neighours, in a sample dataset with size specified as follows
number_of_neighbours = 5
number_of_dimensions = 500
number_of_points = 50000
   
# create random data and hash it in each LSH engine
data = create_random_data(number_of_dimensions = number_of_dimensions, number_of_points = number_of_points)
print "random data size: "+str(data.shape)

# LSH engine with a binary projection hash with 1 projection
rbp = RandomBinaryProjections('rbp', 10)
engine1 = Engine(number_of_dimensions, lshashes=[rbp], vector_filters=[NearestFilter(number_of_neighbours), UniqueFilter()])

# LSH engine with a discretized projection hash with 1 projection and a bit width of 0.05
rdp = RandomDiscretizedProjections('rdp1', 1, 0.5)
engine2 = Engine(number_of_dimensions, lshashes=[rdp], vector_filters=[NearestFilter(number_of_neighbours), UniqueFilter()])

insert_data (data, engine1)
insert_data (data, engine2)

# create random query point
q = np.random.randn(data.shape[1])
print "query size: "+str(q.shape)

# compute the actual distances to the closest neighbours

start_time = time.time()
distances = np.sqrt(np.sum((data-q)**2,axis=1))
elapsed_time = time.time()-start_time
distances_sorted = np.sort(distances)[0:number_of_neighbours]
max_distance_to_closest_neighbours = distances_sorted.max()
print "distances to closest neighbors: ",distances_sorted
print "max distance in dataset:        ", distances.max()
print "time to compute all distances:  ",elapsed_time


# --- NOW WE USE LSH ---
print "-----"
print "----- LSH with binary projections ---- "
# query engine 1
start_time = time.time()
N1 = engine1.neighbours(q)
elapsed_time = time.time()-start_time
# show info about first point found. uncomment next line if you want to see the point coordinates
#print "point coordinates: "+str(N1[0][0])
print "point ids:                ", [N1[i][1] for i in range(len(N1))]
print "distances to query point: ", [N1[i][2] for i in range(len(N1))]
print "time to find neighbours:  ",elapsed_time


print "-----"
print "----- LSH with discretized projections --- "
# query engine 2
start_time = time.time()
N2 = engine2.neighbours(q)
elapsed_time = time.time()-start_time
# show info about first point found. uncomment next line if you want to see the point coordinates
#print "point coordinates: ",N2[0][0]
print "point ids:                ",[N2[i][1] for i in range(len(N2))]
print "distances to query point: ", [N2[i][2] for i in range(len(N2))]
print "time to find neighbours:  ", elapsed_time
